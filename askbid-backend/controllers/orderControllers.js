const connection = require('../models/dbConnection')

const createOrder = (req, res) => {}

const getDetailOrder = (req, res) => {}

const getAskPrice = (req, res) => {}

const getBidPrice = (req, res) => {}

const updateOrder = (req, res) => {}

const deleteOrder = (req, res) => {}

module.exports = {
    createOrder,
    getDetailOrder,
    getAskPrice,
    getBidPrice,
    updateOrder,
    deleteOrder
}
